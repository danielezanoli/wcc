﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using labid.gui;
using System.Reflection;
using ScintillaNET;
using System.Text.RegularExpressions;

namespace WCC
{
	public partial class Form1 : Form, IMRUClient
	{
		const string RESULT_DELIMITER = "___***___";
		const string PLOT_DELIMITER = "@gnuplot";
		const string TAG_OUTPUT = "$OUTPUT";
		
		/// <summary>
		/// Most recently used files manager: manu dei file recenti aperti.
		/// </summary>
		MRUManager mruManager;

		List<string> targetFiles;
		AsyncControlUpdater async;
		StreamWriter currentWriter;
		Process calcProcess;
		StringBuilder messageBuilder;
		string appDataFolder;
		string fileToOpenOnFormLoad = "";
		bool isProcessing = false;
		bool capitalizeHex = false;
		RunOptions runOptions = new RunOptions();
		string plotScript = "";
		string gnuPlotLocation = "";

		public Form1(string calFileToLoad) : this()
		{
			fileToOpenOnFormLoad = calFileToLoad;
		}

		public Form1()
		{
			Form.CheckForIllegalCrossThreadCalls = false;
			Icon = global::WCC.Properties.Resources.Sparkling;

			appDataFolder = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\WCC\\bin";

			if (!Directory.Exists(appDataFolder))
			{
				Directory.CreateDirectory(appDataFolder);
			}

			InitializeComponent();

			// Configuring the default style with properties
			// we have common to every lexer style saves time.
			scintilla1.StyleResetDefault();
			scintilla1.Styles[Style.Default].Font = "Consolas";
			scintilla1.Styles[Style.Default].Size = 10;
			scintilla1.StyleClearAll();

			// Configure the CPP (C#) lexer styles
			scintilla1.Styles[Style.Cpp.Default].ForeColor = Color.Silver;
			scintilla1.Styles[Style.Cpp.Comment].ForeColor = Color.FromArgb(0, 128, 0); // Green
			scintilla1.Styles[Style.Cpp.CommentLine].ForeColor = Color.FromArgb(0, 128, 0); // Green
			scintilla1.Styles[Style.Cpp.CommentLineDoc].ForeColor = Color.FromArgb(128, 128, 128); // Gray
			scintilla1.Styles[Style.Cpp.Number].ForeColor = Color.Olive;
			scintilla1.Styles[Style.Cpp.Word].ForeColor = Color.Blue;
			scintilla1.Styles[Style.Cpp.Word2].ForeColor = Color.Blue;
			scintilla1.Styles[Style.Cpp.String].ForeColor = Color.FromArgb(163, 21, 21); // Red
			scintilla1.Styles[Style.Cpp.Character].ForeColor = Color.FromArgb(163, 21, 21); // Red
			scintilla1.Styles[Style.Cpp.Verbatim].ForeColor = Color.FromArgb(163, 21, 21); // Red
			scintilla1.Styles[Style.Cpp.StringEol].BackColor = Color.Pink;
			scintilla1.Styles[Style.Cpp.Operator].ForeColor = Color.Purple;
			scintilla1.Styles[Style.Cpp.Preprocessor].ForeColor = Color.Maroon;
			scintilla1.Lexer = Lexer.Cpp;

			// Set the keywords
			scintilla1.SetKeywords(0, "define abstract as base break case catch checked continue default delegate do else event explicit extern false finally fixed for foreach goto if implicit in interface internal is lock namespace new null object operator out override params private protected public readonly ref return sealed sizeof stackalloc switch this throw true try typeof unchecked unsafe using virtual while");
			scintilla1.SetKeywords(1, "bool byte char class const decimal double enum float int long sbyte short static string struct uint ulong ushort void");

			// enable line numbers by setting width of margins[0]
			scintilla1.Margins[0].Width = 30;

			async = new AsyncControlUpdater(this);
			targetFiles = new List<string>();
			messageBuilder = new StringBuilder();
		}

		private void Scintilla_InsertCheck(object sender, InsertCheckEventArgs e)
		{
			if (e.Text.EndsWith("\r") || e.Text.EndsWith("\n"))
			{
				int startPos = scintilla1.Lines[scintilla1.LineFromPosition(scintilla1.CurrentPosition)].Position;
				int endPos = e.Position;
				string curLineText = scintilla1.GetTextRange(startPos, (endPos - startPos)); //Text until the caret so that the whitespace is always equal in every line.

				Match indent = Regex.Match(curLineText, "^[ \\t]*");
				e.Text = (e.Text + indent.Value);
				if (Regex.IsMatch(curLineText, "{\\s*$"))
				{
					e.Text = (e.Text + "\t");
				}
			}
		}

		private void Scintilla_CharAdded(object sender, CharAddedEventArgs e)
		{
			//The '}' char.
			if (e.Char == 125)
			{
				int curLine = scintilla1.LineFromPosition(scintilla1.CurrentPosition);

				if (scintilla1.Lines[curLine].Text.Trim() == "}")
				{ //Check whether the bracket is the only thing on the line.. For cases like "if() { }".
					SetIndent(scintilla1, curLine, GetIndent(scintilla1, curLine) - 4);
				}
			}
		}

		//Codes for the handling the Indention of the lines.
		//They are manually added here until they get officially added to the Scintilla control.
		#region "CodeIndent Handlers"
		const int SCI_SETLINEINDENTATION = 2126;
		const int SCI_GETLINEINDENTATION = 2127;
		private void SetIndent(ScintillaNET.Scintilla scin, int line, int indent)
		{
			scin.DirectMessage(SCI_SETLINEINDENTATION, new IntPtr(line), new IntPtr(indent));
		}
		private int GetIndent(ScintillaNET.Scintilla scin, int line)
		{
			return (scin.DirectMessage(SCI_GETLINEINDENTATION, new IntPtr(line), IntPtr.Zero).ToInt32());
		}
		#endregion

		private void control_DragEnter(object sender, DragEventArgs e)
		{
			if (e.Data.GetDataPresent(DataFormats.FileDrop))
				e.Effect = DragDropEffects.Copy;
			else
				e.Effect = DragDropEffects.None;
		}

		private void openCalFileToolStripMenuItem_Click(object sender, EventArgs e)
		{
			if (scintilla1.Modified)
			{
				DialogResult dlg = MessageBox.Show(Path.GetFileName(openFileDialog1.FileName) + " has been modified. Save it?", "Save", MessageBoxButtons.YesNoCancel);

				switch (dlg)
				{
					case DialogResult.Cancel:
						return;

					case DialogResult.Yes:
						saveCalToolStripMenuItem_Click(null, null);
						break;

					default:
						break;
				}
			}

			if (openFileDialog1.ShowDialog() == DialogResult.OK)
			{
				loadCalFile(openFileDialog1.FileName);
			}
		}

		private void loadCalFile(string filename)
		{
			try
			{
				scintilla1.Text = File.ReadAllText(filename);
				//scintilla1.Modified = false;
				openFileDialog1.FileName = filename;
				mruManager.Add(filename);
				Text = Application.ProductName + " - " + Path.GetFileName(filename);
				scintilla1.SetSavePoint();
			}
			catch
			{
				mruManager.Remove(filename);
			}
		}

		private void saveCalToolStripMenuItem_Click(object sender, EventArgs e)
		{
			if (openFileDialog1.FileName != "")
			{
				File.WriteAllText(openFileDialog1.FileName, scintilla1.Text);
				scintilla1.SetSavePoint();

				mruManager.Add(openFileDialog1.FileName);
			}
			else
			{
				saveAsToolStripMenuItem_Click(sender, e);
			}
		}

		private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
		{
			if (saveFileDialog1.ShowDialog() == DialogResult.OK)
			{
				File.WriteAllText(saveFileDialog1.FileName, scintilla1.Text);
				//scintilla1.Modified = false;
				openFileDialog1.FileName = saveFileDialog1.FileName;
				Text = Application.ProductName + " - " + Path.GetFileName(saveFileDialog1.FileName);

				mruManager.Add(saveFileDialog1.FileName);
			}
		}

		/// <summary>
		/// Estrae la parte di script relativa al disegno grafico e restituisce solo la parte
		/// relativa al calcolo
		/// </summary>
		/// <param name="script"></param>
		/// <returns></returns>
		string stripPlotScript(string script)
		{
			int iPlotScript = script.IndexOf(PLOT_DELIMITER);

			if (iPlotScript > -1)
			{
				plotScript = script.Substring(iPlotScript + PLOT_DELIMITER.Length);
				return script.Substring(0, iPlotScript);
			}
			else
			{
				plotScript = "";
				return script;
			}
		}

		void startScript(System.Threading.ThreadStart executionMethod)
		{
			txtOut.Clear();
			messageBuilder = new StringBuilder();

			string scriptText = stripPlotScript(scintilla1.Text);

			scriptText += 
					"\r\n" +
					"if (isnum(.)){\r\n" +
					"print \"" + RESULT_DELIMITER + "\";\r\n" +
					"printf(\"%d\\n\",.);\r\n" +
					"printf(\"%x\\n\",.);\r\n" +
					"printf(\"%b\\n\",.);\r\n" +
					"printf(\"%f\\n\",.);\r\n" +
					"printf(\"%e\\n\",.);\r\n" +
					"printf(\"%r\\n\",.);}";

			//File.WriteAllText(appDataFolder + "\\temp.cal", scriptText);
			File.WriteAllText(Application.StartupPath + "\\temp.cal", scriptText);

			txtFormattedResult.Clear();

			System.Threading.Thread t = new System.Threading.Thread(executionMethod);
			t.Start();
		}

		private void convertFilesToUTF8()
		{
			foreach (string f in targetFiles)
			{
				StreamReader sr = new StreamReader(f);
				sr.ReadLine();
				Encoding enc = sr.CurrentEncoding;
				sr.Close();
				sr.Dispose();

				if (enc == Encoding.UTF8 || enc == Encoding.ASCII || enc == Encoding.UTF7)
				{
					continue;
				}
				else
				{
					message("Converting to UTF-8 file " + Path.GetFileName(f));
					UTF8Converter.FromFile(f);
				}
			}
		}

		void RunWithRedirect(string cmdPath, string args)
		{
			var proc = new Process();

			//proc.StartInfo.StandardOutputEncoding = System.Text.Encoding.UTF8;

			proc.StartInfo.FileName = cmdPath;
			proc.StartInfo.Arguments = args;

			proc.StartInfo.WorkingDirectory = appDataFolder;

			// set up output redirection
			proc.StartInfo.RedirectStandardOutput = true;
			proc.StartInfo.RedirectStandardError = true;
			proc.EnableRaisingEvents = true;
			proc.StartInfo.CreateNoWindow = true;
			proc.StartInfo.UseShellExecute = false;
			// see below for output handler
			proc.ErrorDataReceived += proc_DataReceived;
			proc.OutputDataReceived += proc_DataReceived;

			calcProcess = proc;

			proc.Start();

			proc.BeginErrorReadLine();
			proc.BeginOutputReadLine();

			proc.WaitForExit();
		}

		void proc_DataReceived(object sender, DataReceivedEventArgs e)
		{
			if (e.Data != null)
			{
				if (runOptions.ShowOutput)
					message(e.Data);

				if (currentWriter != null)
					currentWriter.WriteLine(e.Data);
			}
		}

		private void Form1_FormClosing(object sender, FormClosingEventArgs e)
		{
			// si propone il salvataggio del file di script se si era aperto uno script in precedenza
			// oppure se non ne si era aperto nessuno ma si sono inseriti più di 255 caratteri di script
			if (openFileDialog1.FileName != "" || (openFileDialog1.FileName == "" && scintilla1.TextLength > 255))
			{
				if (scintilla1.Modified)
				{
					DialogResult dlg = MessageBox.Show(Path.GetFileName(openFileDialog1.FileName) + " has been modified. Save it?", "Save", MessageBoxButtons.YesNoCancel);

					switch (dlg)
					{
						case DialogResult.Cancel:
							e.Cancel = true;
							break;

						case DialogResult.Yes:
							saveCalToolStripMenuItem_Click(null, null);
							break;

						default:
							break;
					}
				}
			}

			try
			{
				Properties.Settings.Default.Save();
			}
			catch { }
		}

	
		delegate void StringArrayProcessor(string[] str_values);
		delegate void StringProcessor(string str_value);

		private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
		{
			MessageBox.Show("WCC: front end for Calc C-like calculation/scripting language\r\n");
		}

		private void txtOut_DoubleClick(object sender, EventArgs e)
		{
			messageBuilder = new StringBuilder();
			txtOut.Clear();
		}

		private void stopCalcToolStripMenuItem_Click(object sender, EventArgs e)
		{
			isProcessing = false;
			if (calcProcess != null)
			{
				if (!calcProcess.HasExited)
				{
					calcProcess.Kill();
					message("Killing Calc..");
				}
				else
				{
					message("Calc already exited");
				}
			}
			else
			{
				message("Calc not yet executed");
			}
		}

		void message(string msg)
		{
			messageBuilder.Append(msg + Environment.NewLine);
			enableTimer(true);
		}

		private void timer1_Tick(object sender, EventArgs e)
		{
			enableTimer(false);
			txtOut.AppendText(messageBuilder.ToString());
			messageBuilder = new StringBuilder();
		}

		private void enableTimer(bool enabled)
		{
			if (InvokeRequired)
				Invoke(new TimerEnabler(enableTimer), enabled);
			else
				timer1.Enabled = enabled;
		}

		private void calcConsoleToolStripMenuItem_Click(object sender, EventArgs e)
		{
			try
			{
				Process.Start(Application.StartupPath + "\\calc.exe");
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}

		private void mnuNewCalFile_Click(object sender, EventArgs e)
		{
			if (scintilla1.Modified)
			{
				DialogResult dlg = MessageBox.Show(Path.GetFileName(openFileDialog1.FileName) + " has been modified. Save it?", "Save", MessageBoxButtons.YesNoCancel);

				switch (dlg)
				{
					case DialogResult.Cancel:
						return;

					case DialogResult.Yes:
						saveCalToolStripMenuItem_Click(null, null);
						break;

					default:
						break;
				}
			}

			openFileDialog1.FileName = "";
			scintilla1.Text = "";
			//scintilla1.Modified = false;
			Text = Application.ProductName;
		}

		private void runOnFooFile()
		{
			string basePath;
			
			if (openFileDialog1.FileName == "")
			{
				basePath = appDataFolder;
				//basePath = Application.StartupPath;
			}
			else
			{
				basePath = Path.GetDirectoryName(openFileDialog1.FileName);
			}

			string outputFileName = basePath + "\\wcc_out.txt";

			isProcessing = true;

			if (runOptions.NoOutputFile || basePath == string.Empty)
				currentWriter = null;
			else
				currentWriter = new StreamWriter(outputFileName);

			RunWithRedirect(Application.StartupPath + "\\calc.exe",
			        "read temp.cal");

			// calc.exe non accetta il "\" come separatore di percorso, ma il "/"
			
			//RunWithRedirect(Application.StartupPath + "\\calc.exe",
			//		"read \"" + calcAppDataFolder + "/temp.cal\"");

			if (currentWriter != null)
				currentWriter.Close();

			// esegue l'ultimo aggiornamento della finestra di output
			enableTimer(false);
			timer1_Tick(null, null);

			// rimuove dall'output le ultime righe, che contengono il risultato in diversi formati (dec, hex, bin ecc..)
			// e che seguono un delimitatore specifico
			string output = txtOut.Text;
			int iDelimiter = output.IndexOf(RESULT_DELIMITER);
			if (iDelimiter > -1)
			{
				try
				{
					string formattedText = output.Substring(iDelimiter + RESULT_DELIMITER.Length + Environment.NewLine.Length);
					formattedText = formatOutput(formattedText);
					txtFormattedResult.Text = formattedText;
					output = output.Remove(iDelimiter);
					txtOut.Text = output;
				}
				catch (Exception ex) 
				{
					Console.WriteLine(ex.StackTrace); 
				}
			}

			txtOut.AppendText("\r\nDone. --> " + outputFileName);

			isProcessing = false;

			if (plotScript != "")
			{
				RunPlotScript(outputFileName);
			}
		}

		private void RunPlotScript(string outputFileName)
		{
			if (gnuPlotLocation == "")
			{
				DialogBox.showWarning(this, "Please specify a valid location for gnuplot.exe from Options menu");
				return;
			}

			// filtra il file di output eliminando le righe di definizione delle funzioni
			// e ciò che segue il RESULT_DELIMITER
			string filteredOutputFileName = filterOutputFile(outputFileName);

			// sostituisce la variabile $OUTPUT nello script grafico con il file generato da calc.exe
			string plt = plotScript.Replace(TAG_OUTPUT, filteredOutputFileName);// +"\r\npause -1 \"Hit return to continue\"";
			string pltFileName = Application.StartupPath + "\\temp.plt";
			File.WriteAllText(pltFileName, plt);
			Process proc = new Process();
			proc.StartInfo.FileName = gnuPlotLocation;
			proc.StartInfo.Arguments = pltFileName + " -persist";

			proc.StartInfo.WorkingDirectory = Path.GetDirectoryName(pltFileName);
			proc.StartInfo.CreateNoWindow = true;
			proc.StartInfo.UseShellExecute = false;
			// see below for output handler
			proc.Start();
		}

		/// <summary>
		/// Crea una copia del file di output di calc in cui sono omesse le linee di definizione delle funzioni
		/// e quello che segue il delimitatore per l'output delle GUI
		/// </summary>
		/// <param name="outputFileName">Nome e percorso del file di output di calc</param>
		/// <returns>Il nome del file filtrato</returns>
		private string filterOutputFile(string outputFileName)
		{
			string filteredOutput = outputFileName + ".dat";
			StreamWriter sw = new StreamWriter(filteredOutput);
			StreamReader sr = new StreamReader(outputFileName);

			using (sr)
			{
				using (sw)
				{
					string line;

					while ((line = sr.ReadLine()) != null)
					{
						if (line.EndsWith("defined"))
							continue;
						if (line == RESULT_DELIMITER)
							continue;
						sw.WriteLine(line);
					}
				}
			}
			sr.Close();
			sw.Close();

			return filteredOutput;
		}

		private string formatOutput(string formattedText)
		{
			if (formattedText.Contains("Error"))
				return "";

			string[] lines = formattedText.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);

			// formatta la prima riga, aggiungendo i separatori di migliaia
			string sb0 = "";
			
			int iDot = lines[0].IndexOf('.');
			int iEnd = iDot > -1 ? iDot : lines[0].Length;
			string intPart = lines[0].Substring(0, iEnd);
			
			for (int i = 0; i < intPart.Length; i += 3)
			{
				if (intPart.Length > i + 3)
				{
					sb0 = '`' + intPart.Substring(intPart.Length - i - 3, 3) + sb0;
				}
				else
				{
					sb0 = intPart.Substring(0, intPart.Length - i) + sb0;
				}
			}

			// se presente, aggiunge la parte decimale (senza separazione delle migliaia)
			if (iDot > -1)
			{
				sb0 += lines[0].Substring(iDot);
			}

			StringBuilder result = new StringBuilder();

			// come prima riga del risultato si inserisce l'output separato per migliaia
			result.Append(sb0 + Environment.NewLine);

			// si aggiungono tutte le eltre righe e si
			// fa il padding a 8 bit delle stringhe che contengono la rappresentazione binaria ed esadecimale del risultato
			foreach (string line in lines)
			{
				string l = line;

				if (l.StartsWith("0x") && !l.Contains('/'))
				{
					if (l.Length % 2 > 0)
					{
						l = "0x0" + (capitalizeHex ? l.Substring(2).ToUpper() : l.Substring(2));
					}
				}
				else if (l.StartsWith("0b") && !l.Contains('/'))
				{
					int nb = (l.Length-2) % 8;
					if (nb > 0)
					{
						l = "0b" + new string('0', 8 - nb) + l.Substring(2);
					}
				}

				result.Append(l + Environment.NewLine);
			}

			return result.ToString();
		}

		private void initMruManager()
		{
			mruManager = new MRUManager();
			mruManager.Initialize(this, mnuRecentFiles, "Software\\LABID\\" + Application.ProductName);
			mruManager.MaxDisplayNameLength = 80;
			mruManager.MaxMRULength = 10;
		}


		private void Form1_Load(object sender, EventArgs e)
		{
			gnuPlotLocation = Properties.Settings.Default.GnuPlotPath;

			initMruManager();

			if (fileToOpenOnFormLoad != "")
			{
				loadCalFile(fileToOpenOnFormLoad);
			}

			setRunOptionsFromGui();
		}

		private void scintilla1_DragDrop(object sender, DragEventArgs e)
		{
			if (!e.Data.GetDataPresent(DataFormats.FileDrop))
				return;

			try
			{
				Array a = (Array)e.Data.GetData(DataFormats.FileDrop);

				if (a != null)
				{
					string filename = a.GetValue(0).ToString();
					
					this.Activate();        // in the case Explorer overlaps this form
					BeginInvoke(new StringProcessor(loadCalFile), new object[] { filename });
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}

		private void runScriptToolStripMenuItem_Click(object sender, EventArgs e)
		{
			runOptions.RunMode = ERunMode.NoInputFile;
			setRunOptionsFromGui();
			
			foreach (ScintillaNET.Line sline in scintilla1.Lines)
			{
				string line = sline.Text;

				if (line.Trim().StartsWith("#!"))
				{
					if (line.Contains("RUN_ALONE"))
					{
						runOptions.RunMode = ERunMode.NoInputFile;
					}
					else if (line.Contains("RUN_ALL"))
					{
						runOptions.RunMode = ERunMode.AllFiles;
					}
					else if (line.Contains("RUN_SINGLE"))
					{
						runOptions.RunMode = ERunMode.SingleFile;
					}
					else if (line.Contains("OUTPUT_FILE"))
					{
						runOptions.NoOutputFile = (line.Substring(line.IndexOf('=')+1).StartsWith("0"));
					}
					else if (line.Contains("SHOW_OUTPUT"))
					{
						runOptions.ShowOutput = (line.Substring(line.IndexOf('=')+1).StartsWith("1"));
					}
				}
			}

			startScript();
		}

		private void startScript()
		{
			switch (runOptions.RunMode)
			{
				case ERunMode.AllFiles:
					//startScript(new System.Threading.ThreadStart(runOnAllFiles));
					break;

				case ERunMode.SingleFile:
					//startScript(new System.Threading.ThreadStart(runOnEverySingleFile));
					break;

				default:
				case ERunMode.NoInputFile:
					startScript(new System.Threading.ThreadStart(runOnFooFile));
					break;				
			}
		}

		void setRunOptionsFromGui()
		{
			runOptions.NoOutputFile = noOutputFileToolStripMenuItem.Checked;
			runOptions.ShowOutput = mnuShowOutput.Checked;
		}

		enum ERunMode
		{
			SingleFile,
			AllFiles,
			NoInputFile
		}

		class RunOptions
		{
			public ERunMode RunMode = ERunMode.NoInputFile;
			public bool NoOutputFile = false;
			public bool ShowOutput = true;
		}

		private void runScriptingOptionsToolStripMenuItem_Click(object sender, EventArgs e)
		{
			string msg =
				"Execute script shortcut: CTRL+Enter\r\n" +
				"You can insert pre-processor instructions into the  script to set the execution options.\r\n" +
				"Create output file:		#! OUTPUT_FILE=[0/1]\r\n" +
				"Show output:		#! SHOW_OUTPUT=[0/1]\r\n\r\n" +
				"Start gnuplot script:		" + PLOT_DELIMITER + "\r\n" +
				"calc output in gnuplot script:	" + TAG_OUTPUT;

			MessageBox.Show(this, msg);
		}

		private void scintilla1_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Enter && e.Control)
			{
				runScriptToolStripMenuItem_Click(null, null);
			}
			else if (e.KeyCode == Keys.Escape)
			{
				Close();
			}
		}

		private void mnuClearScript_Click(object sender, EventArgs e)
		{
			if (scintilla1.Modified && openFileDialog1.FileName != "")
			{
				mnuNewCalFile_Click(sender, e);
			}
			else
			{
				Text = Application.ProductName;
				scintilla1.ResetText();
				openFileDialog1.FileName = "";
			}
		}

		private void stopScriptToolStripMenuItem_Click(object sender, EventArgs e)
		{
			isProcessing = false;
			if (calcProcess != null)
			{
				if (!calcProcess.HasExited)
				{
					calcProcess.Kill();
					message("Killing Calc..");
				}
				else
				{
					message("Calc already exited");
				}
			}
			else
			{
				message("Calc not yet executed");
			}
		}

		private void capitalizeHexDigitsToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
		{
			capitalizeHex = capitalizeHexDigitsToolStripMenuItem.Checked;
		}

		private void gnuPlotLocationToolStripMenuItem_Click(object sender, EventArgs e)
		{
			OpenFileDialog dlg = new OpenFileDialog();
			dlg.FileName = "gnuplot.exe";

			if (dlg.ShowDialog() == DialogResult.OK)
			{
				gnuPlotLocation = dlg.FileName;
				Properties.Settings.Default.GnuPlotPath = gnuPlotLocation;
			}

			dlg.Dispose();
		}

		public void OpenMRUFile(string fileName)
		{
			if (scintilla1.Modified)
			{
				DialogResult dlg = MessageBox.Show(Path.GetFileName(openFileDialog1.FileName) + " has been modified. Save it?", "Save", MessageBoxButtons.YesNoCancel);

				switch (dlg)
				{
					case DialogResult.Cancel:
						return;

					case DialogResult.Yes:
						saveCalToolStripMenuItem_Click(null, null);
						break;

					default:
						break;
				}
			}

			openFileDialog1.FileName = fileName;
			saveFileDialog1.FileName = fileName;
			loadCalFile(fileName);
		}
	}

	delegate void TimerEnabler(bool enabled);
}
