﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WCC
{
	public partial class NumberedFileForm : Form
	{
		public NumberedFileForm()
		{
			InitializeComponent();
		}

		private void btOK_Click(object sender, EventArgs e)
		{
			if (saveFileDialog1.ShowDialog() == DialogResult.OK)
			{
				createNumberFile(saveFileDialog1.FileName, (int)numericUpDown1.Value, (int)numericUpDown2.Value);
				DialogResult = DialogResult.OK;
				Close();
			}
		}

		private void createNumberFile(string filename, int firstValue, int nLines)
		{
			int currValue = firstValue;

			System.IO.StreamWriter sw = new System.IO.StreamWriter(filename);

			using (sw)
			{
				for (int i = 0; i < nLines; i++)
				{
					sw.WriteLine(currValue.ToString());
					currValue++;
				}
			}
			sw.Close();
		}

		private void btCancel_Click(object sender, EventArgs e)
		{
			DialogResult = DialogResult.Cancel;
			Close();
		}

		public string FileName
		{
			get { return saveFileDialog1.FileName; }
		}

		private void numericUpDown1_Enter(object sender, EventArgs e)
		{
			NumericUpDown source = sender as NumericUpDown;

			source.Select(0, source.Value.ToString().Length);
		}
	}
}
