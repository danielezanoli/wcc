﻿namespace WCC
{
	partial class Form1
	{
		/// <summary>
		/// Variabile di progettazione necessaria.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Liberare le risorse in uso.
		/// </summary>
		/// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Codice generato da Progettazione Windows Form

		/// <summary>
		/// Metodo necessario per il supporto della finestra di progettazione. Non modificare
		/// il contenuto del metodo con l'editor di codice.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.splitContainer1 = new System.Windows.Forms.SplitContainer();
			this.splitContainer2 = new System.Windows.Forms.SplitContainer();
			this.scintilla1 = new ScintillaNET.Scintilla();
			this.txtFormattedResult = new System.Windows.Forms.TextBox();
			this.txtOut = new System.Windows.Forms.TextBox();
			this.menuStrip1 = new System.Windows.Forms.MenuStrip();
			this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.mnuNewAwkFile = new System.Windows.Forms.ToolStripMenuItem();
			this.openAwkFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.saveAwkToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.mnuClearScript = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
			this.mnuRecentFiles = new System.Windows.Forms.ToolStripMenuItem();
			this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.runToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.runWithoutFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.stopScriptToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.mnuShowOutput = new System.Windows.Forms.ToolStripMenuItem();
			this.noOutputFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.capitalizeHexDigitsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.gnuPlotLocationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
			this.aWKManualToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.runScriptingOptionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.stopSpnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
			this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
			this.openFileDialog2 = new System.Windows.Forms.OpenFileDialog();
			this.timer1 = new System.Windows.Forms.Timer(this.components);
			this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
			this.stopAWKToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
			this.splitContainer1.Panel1.SuspendLayout();
			this.splitContainer1.Panel2.SuspendLayout();
			this.splitContainer1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
			this.splitContainer2.Panel1.SuspendLayout();
			this.splitContainer2.Panel2.SuspendLayout();
			this.splitContainer2.SuspendLayout();
			this.menuStrip1.SuspendLayout();
			this.SuspendLayout();
			// 
			// splitContainer1
			// 
			this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer1.Location = new System.Drawing.Point(0, 24);
			this.splitContainer1.Name = "splitContainer1";
			this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
			// 
			// splitContainer1.Panel1
			// 
			this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
			// 
			// splitContainer1.Panel2
			// 
			this.splitContainer1.Panel2.Controls.Add(this.txtOut);
			this.splitContainer1.Size = new System.Drawing.Size(595, 436);
			this.splitContainer1.SplitterDistance = 300;
			this.splitContainer1.TabIndex = 0;
			// 
			// splitContainer2
			// 
			this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer2.Location = new System.Drawing.Point(0, 0);
			this.splitContainer2.Name = "splitContainer2";
			// 
			// splitContainer2.Panel1
			// 
			this.splitContainer2.Panel1.Controls.Add(this.scintilla1);
			// 
			// splitContainer2.Panel2
			// 
			this.splitContainer2.Panel2.Controls.Add(this.txtFormattedResult);
			this.splitContainer2.Size = new System.Drawing.Size(595, 300);
			this.splitContainer2.SplitterDistance = 423;
			this.splitContainer2.TabIndex = 0;
			// 
			// scintilla1
			// 
			this.scintilla1.AllowDrop = true;
			this.scintilla1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.scintilla1.Location = new System.Drawing.Point(0, 0);
			this.scintilla1.Name = "scintilla1";
			this.scintilla1.Size = new System.Drawing.Size(423, 300);
			this.scintilla1.TabIndex = 0;
			this.scintilla1.UseTabs = false;
			this.scintilla1.CharAdded += new System.EventHandler<ScintillaNET.CharAddedEventArgs>(this.Scintilla_CharAdded);
			this.scintilla1.InsertCheck += new System.EventHandler<ScintillaNET.InsertCheckEventArgs>(this.Scintilla_InsertCheck);
			this.scintilla1.DragDrop += new System.Windows.Forms.DragEventHandler(this.scintilla1_DragDrop);
			this.scintilla1.DragEnter += new System.Windows.Forms.DragEventHandler(this.control_DragEnter);
			this.scintilla1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.scintilla1_KeyDown);
			// 
			// txtFormattedResult
			// 
			this.txtFormattedResult.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtFormattedResult.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtFormattedResult.Location = new System.Drawing.Point(0, 0);
			this.txtFormattedResult.Multiline = true;
			this.txtFormattedResult.Name = "txtFormattedResult";
			this.txtFormattedResult.ScrollBars = System.Windows.Forms.ScrollBars.Both;
			this.txtFormattedResult.Size = new System.Drawing.Size(168, 300);
			this.txtFormattedResult.TabIndex = 2;
			this.toolTip1.SetToolTip(this.txtFormattedResult, "SPN output");
			this.txtFormattedResult.WordWrap = false;
			// 
			// txtOut
			// 
			this.txtOut.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtOut.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtOut.Location = new System.Drawing.Point(0, 0);
			this.txtOut.Multiline = true;
			this.txtOut.Name = "txtOut";
			this.txtOut.ScrollBars = System.Windows.Forms.ScrollBars.Both;
			this.txtOut.Size = new System.Drawing.Size(595, 132);
			this.txtOut.TabIndex = 1;
			this.toolTip1.SetToolTip(this.txtOut, "SPN output");
			this.txtOut.WordWrap = false;
			// 
			// menuStrip1
			// 
			this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.runToolStripMenuItem,
            this.optionsToolStripMenuItem,
            this.toolStripMenuItem1});
			this.menuStrip1.Location = new System.Drawing.Point(0, 0);
			this.menuStrip1.Name = "menuStrip1";
			this.menuStrip1.Size = new System.Drawing.Size(595, 24);
			this.menuStrip1.TabIndex = 1;
			this.menuStrip1.Text = "menuStrip1";
			// 
			// fileToolStripMenuItem
			// 
			this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuNewAwkFile,
            this.openAwkFileToolStripMenuItem,
            this.saveAwkToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.mnuClearScript,
            this.toolStripSeparator2,
            this.mnuRecentFiles,
            this.closeToolStripMenuItem});
			this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
			this.fileToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
			this.fileToolStripMenuItem.Text = "CAL file";
			// 
			// mnuNewAwkFile
			// 
			this.mnuNewAwkFile.Name = "mnuNewAwkFile";
			this.mnuNewAwkFile.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
			this.mnuNewAwkFile.Size = new System.Drawing.Size(212, 22);
			this.mnuNewAwkFile.Text = "New";
			this.mnuNewAwkFile.Click += new System.EventHandler(this.mnuNewCalFile_Click);
			// 
			// openAwkFileToolStripMenuItem
			// 
			this.openAwkFileToolStripMenuItem.Name = "openAwkFileToolStripMenuItem";
			this.openAwkFileToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
			this.openAwkFileToolStripMenuItem.Size = new System.Drawing.Size(212, 22);
			this.openAwkFileToolStripMenuItem.Text = "Open";
			this.openAwkFileToolStripMenuItem.Click += new System.EventHandler(this.openCalFileToolStripMenuItem_Click);
			// 
			// saveAwkToolStripMenuItem
			// 
			this.saveAwkToolStripMenuItem.Name = "saveAwkToolStripMenuItem";
			this.saveAwkToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
			this.saveAwkToolStripMenuItem.Size = new System.Drawing.Size(212, 22);
			this.saveAwkToolStripMenuItem.Text = "Save";
			this.saveAwkToolStripMenuItem.Click += new System.EventHandler(this.saveCalToolStripMenuItem_Click);
			// 
			// saveAsToolStripMenuItem
			// 
			this.saveAsToolStripMenuItem.Enabled = false;
			this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
			this.saveAsToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.S)));
			this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(212, 22);
			this.saveAsToolStripMenuItem.Text = "Save as";
			this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.saveAsToolStripMenuItem_Click);
			// 
			// mnuClearScript
			// 
			this.mnuClearScript.Name = "mnuClearScript";
			this.mnuClearScript.ShortcutKeys = System.Windows.Forms.Keys.F8;
			this.mnuClearScript.Size = new System.Drawing.Size(212, 22);
			this.mnuClearScript.Text = "Clear";
			this.mnuClearScript.Click += new System.EventHandler(this.mnuClearScript_Click);
			// 
			// toolStripSeparator2
			// 
			this.toolStripSeparator2.Name = "toolStripSeparator2";
			this.toolStripSeparator2.Size = new System.Drawing.Size(209, 6);
			// 
			// mnuRecentFiles
			// 
			this.mnuRecentFiles.Name = "mnuRecentFiles";
			this.mnuRecentFiles.Size = new System.Drawing.Size(212, 22);
			this.mnuRecentFiles.Text = "Recent files";
			// 
			// closeToolStripMenuItem
			// 
			this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
			this.closeToolStripMenuItem.Size = new System.Drawing.Size(212, 22);
			this.closeToolStripMenuItem.Text = "Close";
			// 
			// runToolStripMenuItem
			// 
			this.runToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.runWithoutFileToolStripMenuItem,
            this.stopScriptToolStripMenuItem});
			this.runToolStripMenuItem.Name = "runToolStripMenuItem";
			this.runToolStripMenuItem.Size = new System.Drawing.Size(40, 20);
			this.runToolStripMenuItem.Text = "Run";
			// 
			// runWithoutFileToolStripMenuItem
			// 
			this.runWithoutFileToolStripMenuItem.Name = "runWithoutFileToolStripMenuItem";
			this.runWithoutFileToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F5;
			this.runWithoutFileToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
			this.runWithoutFileToolStripMenuItem.Text = "Run script";
			this.runWithoutFileToolStripMenuItem.Click += new System.EventHandler(this.runScriptToolStripMenuItem_Click);
			// 
			// stopScriptToolStripMenuItem
			// 
			this.stopScriptToolStripMenuItem.Name = "stopScriptToolStripMenuItem";
			this.stopScriptToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F4)));
			this.stopScriptToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
			this.stopScriptToolStripMenuItem.Text = "Stop script";
			this.stopScriptToolStripMenuItem.Click += new System.EventHandler(this.stopScriptToolStripMenuItem_Click);
			// 
			// optionsToolStripMenuItem
			// 
			this.optionsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuShowOutput,
            this.noOutputFileToolStripMenuItem,
            this.capitalizeHexDigitsToolStripMenuItem,
            this.gnuPlotLocationToolStripMenuItem});
			this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
			this.optionsToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
			this.optionsToolStripMenuItem.Text = "Options";
			// 
			// mnuShowOutput
			// 
			this.mnuShowOutput.Checked = global::WCC.Properties.Settings.Default.ShowOutput;
			this.mnuShowOutput.CheckOnClick = true;
			this.mnuShowOutput.CheckState = System.Windows.Forms.CheckState.Checked;
			this.mnuShowOutput.Name = "mnuShowOutput";
			this.mnuShowOutput.Size = new System.Drawing.Size(181, 22);
			this.mnuShowOutput.Text = "Show output";
			// 
			// noOutputFileToolStripMenuItem
			// 
			this.noOutputFileToolStripMenuItem.CheckOnClick = true;
			this.noOutputFileToolStripMenuItem.Name = "noOutputFileToolStripMenuItem";
			this.noOutputFileToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
			this.noOutputFileToolStripMenuItem.Text = "No output file";
			// 
			// capitalizeHexDigitsToolStripMenuItem
			// 
			this.capitalizeHexDigitsToolStripMenuItem.Checked = global::WCC.Properties.Settings.Default.CapitalizeHexDigits;
			this.capitalizeHexDigitsToolStripMenuItem.CheckOnClick = true;
			this.capitalizeHexDigitsToolStripMenuItem.Name = "capitalizeHexDigitsToolStripMenuItem";
			this.capitalizeHexDigitsToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
			this.capitalizeHexDigitsToolStripMenuItem.Text = "Capitalize Hex digits";
			this.capitalizeHexDigitsToolStripMenuItem.CheckedChanged += new System.EventHandler(this.capitalizeHexDigitsToolStripMenuItem_CheckedChanged);
			// 
			// gnuPlotLocationToolStripMenuItem
			// 
			this.gnuPlotLocationToolStripMenuItem.Name = "gnuPlotLocationToolStripMenuItem";
			this.gnuPlotLocationToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
			this.gnuPlotLocationToolStripMenuItem.Text = "GnuPlot location";
			this.gnuPlotLocationToolStripMenuItem.Click += new System.EventHandler(this.gnuPlotLocationToolStripMenuItem_Click);
			// 
			// toolStripMenuItem1
			// 
			this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aWKManualToolStripMenuItem,
            this.runScriptingOptionsToolStripMenuItem,
            this.aboutToolStripMenuItem});
			this.toolStripMenuItem1.Name = "toolStripMenuItem1";
			this.toolStripMenuItem1.Size = new System.Drawing.Size(24, 20);
			this.toolStripMenuItem1.Text = "?";
			// 
			// aWKManualToolStripMenuItem
			// 
			this.aWKManualToolStripMenuItem.Name = "aWKManualToolStripMenuItem";
			this.aWKManualToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F1;
			this.aWKManualToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
			this.aWKManualToolStripMenuItem.Text = "Calc console";
			this.aWKManualToolStripMenuItem.Click += new System.EventHandler(this.calcConsoleToolStripMenuItem_Click);
			// 
			// runScriptingOptionsToolStripMenuItem
			// 
			this.runScriptingOptionsToolStripMenuItem.Name = "runScriptingOptionsToolStripMenuItem";
			this.runScriptingOptionsToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
			this.runScriptingOptionsToolStripMenuItem.Text = "Run scripting help";
			this.runScriptingOptionsToolStripMenuItem.Click += new System.EventHandler(this.runScriptingOptionsToolStripMenuItem_Click);
			// 
			// aboutToolStripMenuItem
			// 
			this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
			this.aboutToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
			this.aboutToolStripMenuItem.Text = "Info";
			this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
			// 
			// stopSpnToolStripMenuItem
			// 
			this.stopSpnToolStripMenuItem.Name = "stopSpnToolStripMenuItem";
			this.stopSpnToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
			this.stopSpnToolStripMenuItem.Text = "Stop SPN process";
			this.stopSpnToolStripMenuItem.Click += new System.EventHandler(this.stopCalcToolStripMenuItem_Click);
			// 
			// openFileDialog1
			// 
			this.openFileDialog1.Filter = "WCC files|*.cal|Tutti file|*.*";
			// 
			// saveFileDialog1
			// 
			this.saveFileDialog1.DefaultExt = "spn";
			this.saveFileDialog1.Filter = "WCC files|*.cal|All files|*.*";
			// 
			// openFileDialog2
			// 
			this.openFileDialog2.FileName = "openFileDialog2";
			this.openFileDialog2.Multiselect = true;
			// 
			// timer1
			// 
			this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
			// 
			// stopAWKToolStripMenuItem
			// 
			this.stopAWKToolStripMenuItem.Name = "stopAWKToolStripMenuItem";
			this.stopAWKToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(595, 460);
			this.Controls.Add(this.splitContainer1);
			this.Controls.Add(this.menuStrip1);
			this.MainMenuStrip = this.menuStrip1;
			this.Name = "Form1";
			this.Text = "WCC";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
			this.Load += new System.EventHandler(this.Form1_Load);
			this.splitContainer1.Panel1.ResumeLayout(false);
			this.splitContainer1.Panel2.ResumeLayout(false);
			this.splitContainer1.Panel2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
			this.splitContainer1.ResumeLayout(false);
			this.splitContainer2.Panel1.ResumeLayout(false);
			this.splitContainer2.Panel2.ResumeLayout(false);
			this.splitContainer2.Panel2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
			this.splitContainer2.ResumeLayout(false);
			this.menuStrip1.ResumeLayout(false);
			this.menuStrip1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.SplitContainer splitContainer1;
		private System.Windows.Forms.SplitContainer splitContainer2;
		private System.Windows.Forms.MenuStrip menuStrip1;
		private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem openAwkFileToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem saveAwkToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem runToolStripMenuItem;
		private System.Windows.Forms.OpenFileDialog openFileDialog1;
		private System.Windows.Forms.SaveFileDialog saveFileDialog1;
		private System.Windows.Forms.OpenFileDialog openFileDialog2;
		private System.Windows.Forms.TextBox txtOut;
		private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
		private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem mnuShowOutput;
		private System.Windows.Forms.ToolStripMenuItem stopSpnToolStripMenuItem;
		private System.Windows.Forms.Timer timer1;
		private System.Windows.Forms.ToolStripMenuItem aWKManualToolStripMenuItem;
		private System.Windows.Forms.ToolTip toolTip1;
		private System.Windows.Forms.ToolStripMenuItem mnuNewAwkFile;
		private System.Windows.Forms.ToolStripMenuItem runWithoutFileToolStripMenuItem;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
		private System.Windows.Forms.ToolStripMenuItem mnuRecentFiles;
		private System.Windows.Forms.ToolStripMenuItem runScriptingOptionsToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem stopAWKToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem noOutputFileToolStripMenuItem;
		private System.Windows.Forms.TextBox txtFormattedResult;
		private System.Windows.Forms.ToolStripMenuItem mnuClearScript;
		private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem stopScriptToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem capitalizeHexDigitsToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem gnuPlotLocationToolStripMenuItem;
		private ScintillaNET.Scintilla scintilla1;
	}
}

