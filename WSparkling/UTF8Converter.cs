﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace WCC
{
	class UTF8Converter
	{
		const int BLOCK_SIZE = 32768;

		public static void FromFile(string filename)
		{
			string tmpFileName = filename + ".tmp";

			using (StreamReader sr = new StreamReader(filename))
			{
				using (StreamWriter sw = new StreamWriter(tmpFileName, false, Encoding.UTF8))
				{
					int nCharRead;
					char[] buffer = new char[BLOCK_SIZE];

					do
					{
						nCharRead = sr.ReadBlock(buffer, 0, BLOCK_SIZE);
						sw.Write(buffer, 0, nCharRead);
					} while (nCharRead > 0);

					sw.Close();
				}

				sr.Close();
			}

			File.Replace(tmpFileName, filename, null);
		}
	}
}
