﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace WCC
{
	static class Program
	{
		/// <summary>
		/// Punto di ingresso principale dell'applicazione.
		/// </summary>
		[STAThread]
		static void Main(string[] args)
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			Form1 f;

			if (args.Length > 0)
				f = new Form1(args[0]);
			else
				f = new Form1();

			Application.Run(f);
		}
	}
}
